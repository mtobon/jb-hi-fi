# JB Hi-Fi Technical Challenge

## Description
The solution was created using Microsoft Visual Studio 2022 Community edition and consists of three projects.

* **JbHiFi.WebApi**: Web API project containing the REST endpoints
* **JbHiFi.Core**: Project encapsulating all the domain logic
* **JbHiFi.Core.Tests**: Unit tests for **JbHiFi.Core**
* **JbHiFi.WebApi.Tests**: Unit/Integration tests for **JbHiFi.WebApi**
* **JbHiFi.Tests**: Simple web front-end written in Angular to query the back-end APIs

## Packages:
- **JbHiFi.WebApi**: The web API project uses the following third-party packages:
    - _Microsoft DI_ as the IoC container
    - _Scrutor_ to facilitate dependency discovery
    - _AspNetCoreRateLimit_ for rate limiting based on an ApiKey
    - _MediatR_ for command handling.

- **JbHiFi.Tests**:
    - xUnit as the testing framework
    - Moq (mocking library)

- **JbHiFi.Web**:
    - Vanilla Angular
    - Bootstrap

## API Keys:
- Below is the list of available API Keys for the demo. These are located in the `StaticApiStoreReadService` class as static strings. With the current structure, we could easily implement a new class to resolve ApiKeys from any store we want.
    - 0A5B1F6E
    - 93A326C4
    - 52537228
    - 4E6152B5
    - 13B69DE2

## Build Process:
The build process for the solution can be done as follows.

* **Back-end**: Set the **JbHiFi.WebApi** project as the start-up project in Visual Studio and run from there.
* **Front-end**: Open the **JbHiFi.Web** folder, open a command prompt (or PowerShell), and run the `npm install` command. Once installation is complete, run the `npm run start` command. Open `http://localhost:4200/` in a new browser window as soon as the compilation is completed.


## Tests:
The solution contains a couple of Unit and e2e tests in both the front and back-end. The back-end tests can be executed from the Test Explorer in Visual Studio.