export const environment = {
  production: true,
  apiBaseUrl: 'PROD API URL HERE',
  apiKey: 'PROD API KEY HERE'
};
