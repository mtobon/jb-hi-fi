import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class AppHttpInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Set base url and X-Api-Key header
    req = req.clone({
      url: `${environment.apiBaseUrl}/${req.url}`,
      headers: req.headers.append('X-Api-Key', environment.apiKey)
    });

    return next.handle(req);
  }
}
