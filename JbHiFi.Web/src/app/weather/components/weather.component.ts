import { Component } from '@angular/core';
import { finalize } from 'rxjs';
import { WeatherModel } from '../models/weather.model';
import { WeatherApiService } from '../services/weather-api.service';

@Component({
  selector: 'weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent {
  isLoading: boolean = false;
  hasError: boolean = false;

  cityName: string = '';
  countryCode: string = '';

  model?: WeatherModel;

  constructor(private weatherApiService: WeatherApiService) {
  }

  onSubmit() {
    this.isLoading = true;

    this.weatherApiService.getWeather(this.cityName, this.countryCode)
      .pipe(
        finalize(() => this.isLoading = false)
      )
      .subscribe(model => {
        this.hasError = false;
        this.model = model;
      }, error => {
        this.hasError = true;
        this.model = undefined;
      });
  }
}
