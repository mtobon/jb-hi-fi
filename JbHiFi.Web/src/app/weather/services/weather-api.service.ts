import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { WeatherModel } from '../models/weather.model';

@Injectable({
  providedIn: 'root',
})
export class WeatherApiService {
  constructor(private http: HttpClient) {

  }

  getWeather(cityName: string, countryCode: string): Observable<WeatherModel> {
    if (!cityName) {
      return of();
    }

    const queryParams = new URLSearchParams();
    queryParams.set('cityName', encodeURIComponent(cityName));

    if (countryCode) {
      queryParams.set('countryCode', encodeURIComponent(countryCode));
    }

    return this.http.get<WeatherModel>(`weather?${queryParams}`);
  }
}
