﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using JbHiFi.Core;
using JbHiFi.Core.ApiKeyStore;
using JbHiFi.Core.Caching;

namespace JbHiFi.WebApi.Tests
{
    public class TestsStartUp
    {
        public IServiceCollection ServiceCollection { get; }
        public IServiceProvider ServiceProvider { get; }
        public IConfiguration Configuration { get; }

        public TestsStartUp()
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appSettings.json")
                .Build();

            var services = new ServiceCollection();
            ConfigureServices(services);

            ServiceCollection = services;
            ServiceProvider = services.BuildServiceProvider();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IConfiguration>(Configuration);

            services.AddMediatR(typeof(Startup), typeof(IAmDiscoverable));
            services.AddHttpClient();
            services.AddMemoryCache();

            // Add controllers manually
            services.Scan(s => s.FromAssemblyOf<Startup>()
                .AddClasses(c => c.AssignableTo<ControllerBase>())
                .AsSelf()
                .WithScopedLifetime()
            );

            // Add all the application services
            services.AddSingleton<IApiStoreReadService, StaticApiStoreReadService>();
            services.AddSingleton<ICacheManager, MemoryCacheManager>();

            services.Scan(s => s.FromAssemblyOf<IAmDiscoverable>()
                .AddClasses(c => c.AssignableTo<IAmDiscoverable>())
                .AsImplementedInterfaces()
                .WithScopedLifetime()
            );
        }
    }
}
