﻿using JbHiFi.WebApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Xunit;

namespace JbHiFi.WebApi.Tests.Weather
{
    public class GetWeatherSadPath : TestsStartUp
    {
        [Fact]
        public async Task Execute()
        {
            var controller = ServiceProvider.GetService<WeatherController>();
            var result = await controller.Get(null, null);

            Assert.NotNull(result);
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
