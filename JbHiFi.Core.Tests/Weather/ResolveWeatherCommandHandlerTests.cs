﻿using JbHiFi.Core.Weather;
using JbHiFi.Core.Weather.OpenWeather;
using Moq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace JbHiFi.Core.Tests.Weather
{
    public class ResolveWeatherCommandHandlerTests
    {
        [Fact]
        public async Task Handle_WhenOpenWeatherServiceReturnsNull_ReturnsNull()
        {
            var openWeatherService = new Mock<IOpenWeatherService>();
            openWeatherService.Setup(s => s.GetWeather(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync((WeatherResponseDto)null);

            var resolveWeatherCommandHandler = new ResolveWeatherCommandHandler(openWeatherService.Object);
            var result = await resolveWeatherCommandHandler.Handle(new ResolveWeatherCommand("uk", "London"), CancellationToken.None);

            Assert.Null(result);
        }

        [Fact]
        public async Task Handle_WhenOpenWeatherServiceReturnsAValidResponse_ReturnsTheFirstDescription()
        {
            var openWeatherService = new Mock<IOpenWeatherService>();
            openWeatherService.Setup(s => s.GetWeather(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new WeatherResponseDto
            {
                Weather = new[]
                {
                    new WeatherDto { Description = "First description" },
                    new WeatherDto { Description = "Second description" }
                }
            });

            var resolveWeatherCommandHandler = new ResolveWeatherCommandHandler(openWeatherService.Object);
            var result = await resolveWeatherCommandHandler.Handle(new ResolveWeatherCommand("uk", "London"), CancellationToken.None);

            Assert.NotNull(result);
            Assert.Equal("First description", result.Description);
        }
    }
}
