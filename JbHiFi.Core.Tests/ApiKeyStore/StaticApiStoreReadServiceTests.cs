﻿using System.Threading.Tasks;
using JbHiFi.Core.ApiKeyStore;
using Xunit;

namespace JbHiFi.Core.Tests.ApiKeyStore
{
    public class StaticApiStoreReadServiceTests
    {
        private const string ApiKey = "0A5B1F6E";
        private readonly StaticApiStoreReadService _staticApiStoreReadService;

        public StaticApiStoreReadServiceTests()
        {
            _staticApiStoreReadService = new StaticApiStoreReadService();
        }

        [Fact]
        public async Task IsValidApiKey_WhenProvidingAnInvalidApiKey_ReturnsFalse()
        {
            var result = await _staticApiStoreReadService.IsValidApiKey("abc123");
            Assert.False(result);
        }

        [Fact]
        public async Task IsValidApiKey_WhenProvidingAValidApiKey_ReturnsTrue()
        {
            var result = await _staticApiStoreReadService.IsValidApiKey(ApiKey);
            Assert.True(result);
        }

        [Fact]
        public async Task IsValidApiKey_WhenProvidingAValidApiKeyLowercase_ReturnsTrue()
        {
            var result = await _staticApiStoreReadService.IsValidApiKey(ApiKey.ToLower());
            Assert.True(result);
        }
    }
}
