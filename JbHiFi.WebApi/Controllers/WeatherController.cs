﻿using JbHiFi.Core.Weather;
using JbHiFi.WebApi.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace JbHiFi.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherController : ControllerBase
    {
        private readonly IMediator _mediator;

        public WeatherController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string countryCode, string cityName)
        {
            var response = await _mediator.Send(new ResolveWeatherCommand(countryCode, cityName));

            if (!response.RecordFound)
            {
                return NotFound();
            }

            return Ok(GetWeatherViewModel.FromCommandResponse(response));
        }
    }
}
