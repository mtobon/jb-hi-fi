using AspNetCoreRateLimit;
using JbHiFi.Core;
using JbHiFi.Core.ApiKeyStore;
using JbHiFi.Core.Caching;
using JbHiFi.WebApi.Middleware;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace JbHiFi.WebApi
{
    public class Startup
    {
        const string CorsDevPolicy = "CorsDevPolicy";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Load configuration from appsettings.json
            services.AddOptions();

            // Store rate limit counters and rules
            services.AddMemoryCache();

            // Load configuration from appsettings.json
            services.Configure<ClientRateLimitOptions>(Configuration.GetSection("ClientRateLimiting"));

            // Inject counter and rules stores
            services.AddInMemoryRateLimiting();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();

            services.AddCors(options => {
                options.AddPolicy(CorsDevPolicy, builder => {
                    builder.WithOrigins("http://localhost:4200").AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.AddMediatR(typeof(Startup), typeof(IAmDiscoverable));
            services.AddControllers();
            services.AddHttpClient();

            // Add all the application services
            services.AddSingleton<IApiStoreReadService, StaticApiStoreReadService>();
            services.AddSingleton<ICacheManager, MemoryCacheManager>();

            services.Scan(s => s.FromAssemblyOf<IAmDiscoverable>()
                .AddClasses(c => c.AssignableTo<IAmDiscoverable>())
                .AsImplementedInterfaces()
                .WithScopedLifetime()
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(CorsDevPolicy);
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseClientRateLimiting();

            // Register custom middleware
            app.UseMiddleware<GlobalExceptionHandlerMiddleware>();
            app.UseMiddleware<ApiKeyValidatorMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
