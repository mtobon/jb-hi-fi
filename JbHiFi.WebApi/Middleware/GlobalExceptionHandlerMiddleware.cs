﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;

namespace JbHiFi.WebApi.Middleware
{
    public class GlobalExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public GlobalExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (System.Exception)
            {
                //We can add logging / tracing here if required

                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                var responseContent = JsonConvert.SerializeObject(new
                {
                    StatusCode = context.Response.StatusCode,
                    Message = "An error occurred while processing the request. Please try again later."
                });

                await context.Response.WriteAsync(responseContent);
            }
        }
    }
}
