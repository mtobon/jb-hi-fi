﻿using JbHiFi.Core.ApiKeyStore;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;

namespace JbHiFi.WebApi.Middleware
{
    public class ApiKeyValidatorMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _configuration;
        private readonly IApiStoreReadService _apiStoreReadService;

        public ApiKeyValidatorMiddleware(RequestDelegate next, IConfiguration configuration, IApiStoreReadService apiStoreReadService)
        {
            _next = next;
            _configuration = configuration;
            _apiStoreReadService = apiStoreReadService;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var apiKeyHeaderName = _configuration.GetValue<string>("ClientRateLimiting:ClientIdHeader");

            if (string.IsNullOrWhiteSpace(apiKeyHeaderName) || !context.Request.Headers.ContainsKey(apiKeyHeaderName))
            {
                await HandleBadRequest();
                return;
            }

            var isValidApiKey = await _apiStoreReadService.IsValidApiKey(context.Request.Headers[apiKeyHeaderName]);

            if (!isValidApiKey)
            {
                await HandleBadRequest();
                return;
            }

            // Call the next delegate/middleware in the pipeline.
            await _next(context);

            async Task HandleBadRequest()
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                var responseContent = JsonConvert.SerializeObject(new
                {
                    StatusCode = context.Response.StatusCode,
                    Message = "An API key was not found on the request headers or it is invalid"
                });

                await context.Response.WriteAsync(responseContent);
            }
        }
    }
}
