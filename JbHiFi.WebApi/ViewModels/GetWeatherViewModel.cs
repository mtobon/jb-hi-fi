﻿using JbHiFi.Core.Weather;

namespace JbHiFi.WebApi.ViewModels
{
    public class GetWeatherViewModel
    {
        public string Description { get; private set; }

        public static GetWeatherViewModel FromCommandResponse(ResolveWeatherCommandResponse response)
        {
            return new GetWeatherViewModel
            {
                Description = response.Description
            };
        }
    }
}
