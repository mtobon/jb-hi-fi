﻿using System.Threading.Tasks;

namespace JbHiFi.Core.ApiKeyStore
{
    public interface IApiStoreReadService
    {
        Task<bool> IsValidApiKey(string apiKey);
    }
}
