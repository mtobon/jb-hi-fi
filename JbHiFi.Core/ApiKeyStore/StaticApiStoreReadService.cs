﻿using System.Linq;
using System.Threading.Tasks;

namespace JbHiFi.Core.ApiKeyStore
{
    public class StaticApiStoreReadService : IApiStoreReadService
    {
        public static string[] ApiKeys =
        {
            "0A5B1F6E",
            "93A326C4",
            "52537228",
            "4E6152B5",
            "13B69DE2"
        };

        public Task<bool> IsValidApiKey(string apiKey)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
            {
                return Task.FromResult(false);
            }

            var isValidApiKey = ApiKeys.Any(a => a.Equals(apiKey, System.StringComparison.InvariantCultureIgnoreCase));
            return Task.FromResult(isValidApiKey);
        }
    }
}
