﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;

namespace JbHiFi.Core.Caching
{
    public class MemoryCacheManager : ICacheManager
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;
        private readonly int _expirationInMinutes;

        public MemoryCacheManager(IMemoryCache memoryCache, IConfiguration configuration)
        {
            _memoryCache = memoryCache;
            _configuration = configuration;
            _expirationInMinutes = _configuration.GetValue<int>("CacheManager:SlidingExpirationInMinutes");
        }

        public bool TryGetValue<T>(string key, out T value)
        {
            return _memoryCache.TryGetValue<T>(key, out value);
        }

        public void Set<T>(string key, T value)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(_expirationInMinutes));

            _memoryCache.Set<T>(key, value, cacheEntryOptions);
        }
    }
}
