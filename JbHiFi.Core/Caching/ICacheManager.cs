﻿namespace JbHiFi.Core.Caching
{
    public interface ICacheManager
    {
        bool TryGetValue<T>(string key, out T value);
        void Set<T>(string key, T value);
    }
}
