﻿using JbHiFi.Core.Weather.OpenWeather;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace JbHiFi.Core.Weather
{
    public class ResolveWeatherCommand : IRequest<ResolveWeatherCommandResponse>
    {
        public ResolveWeatherCommand(string countryCode, string cityName)
        {
            CountryCode = countryCode;
            CityName = cityName;
        }

        public string CountryCode { get; }
        public string CityName { get; }
    }

    public class ResolveWeatherCommandResponse
    {
        public ResolveWeatherCommandResponse()
        {
            RecordFound = false;
        }

        public ResolveWeatherCommandResponse(string description)
        {
            RecordFound = !string.IsNullOrWhiteSpace(description);
            Description = description;
        }

        public bool RecordFound { get; }
        public string Description { get; }
    }

    public class ResolveWeatherCommandHandler : IRequestHandler<ResolveWeatherCommand, ResolveWeatherCommandResponse>
    {
        private readonly IOpenWeatherService _openWeatherService;

        public ResolveWeatherCommandHandler(IOpenWeatherService openWeatherService)
        {
            _openWeatherService = openWeatherService;
        }

        public async Task<ResolveWeatherCommandResponse> Handle(ResolveWeatherCommand request, CancellationToken cancellationToken)
        {
            var weather = await _openWeatherService.GetWeather(request.CountryCode, request.CityName);

            if (weather == null)
            {
                return new ResolveWeatherCommandResponse();
            }

            var description = weather.Weather.Select(w => w.Description).FirstOrDefault();
            return new ResolveWeatherCommandResponse(description);
        }
    }
}
