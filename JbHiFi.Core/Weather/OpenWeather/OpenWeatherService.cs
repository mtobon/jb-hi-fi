﻿using JbHiFi.Core.Caching;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Web;

namespace JbHiFi.Core.Weather.OpenWeather
{
    public interface IOpenWeatherService : IAmDiscoverable
    {
        Task<WeatherResponseDto> GetWeather(string countryCode, string cityName);
    }

    public class OpenWeatherService : IOpenWeatherService
    {
        private readonly ICacheManager _cacheManager;
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _httpClientFactory;

        public OpenWeatherService(ICacheManager cacheManager, IConfiguration configuration, IHttpClientFactory httpClientFactory)
        {
            _cacheManager = cacheManager;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<WeatherResponseDto> GetWeather(string countryCode, string cityName)
        {
            if (string.IsNullOrWhiteSpace(cityName))
            {
                return null;
            }

            var filter = !string.IsNullOrWhiteSpace(countryCode) ? $"{cityName},{countryCode}" : cityName;

            if (_cacheManager.TryGetValue<WeatherResponseDto>(filter, out var weatherResponse))
            {
                return weatherResponse;
            }

            var openWeatherConfig = _configuration.GetSection("OpenWeatherMap");

            using var httpClient = _httpClientFactory.CreateClient();
            httpClient.BaseAddress = openWeatherConfig.GetValue<Uri>("BaseUrl");

            var apiKey = openWeatherConfig.GetValue<string>("ApiKey");
            var requestUrl = HttpUtility.UrlPathEncode($"weather?q={filter}&appid={apiKey}");

            var response = await httpClient.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            weatherResponse = await response.Content.ReadFromJsonAsync<WeatherResponseDto>();
            _cacheManager.Set(filter, weatherResponse);

            return weatherResponse;
        }
    }
}
